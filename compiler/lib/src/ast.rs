use crate::token::Token;

pub struct Ast<'a> {
    pub file_name: String,
    pub statements: Vec<Statement<'a>>,
}
pub struct Statement<'a> {
    pub token: Token<'a>,
    //  pub node: StatementNode,
}
// you get the idea here...
// pub enum StatementNode {
//     Let(LetStatement),
//     Return(ReturnStatement),
//     Expression(ExpressionStatement),
// }
