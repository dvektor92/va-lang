pub struct Token<'a> {
    pub file_name: &'a str,
    pub token_type: TokenType,
    pub line: u32,
    pub loc: u32,
}
//
// to give you an idea of how to use this
pub enum TokenType {
    Illegal,
    EOF,
    // Identifiers + literals
    Ident,
    Int(i64), // you can have these hold the values they represent
    Float(f32),
    Literal(String), // I do recommend you use an owned string.. but this is an example. definitely study lifetimes before trying to do too much with slices.
    // Operators
    Assign,
    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,
    Lt,
    Gt,
    Eq,
    NotEq,
    // Delimeters
    Comma,
    Semicolon,
    LParen,
    RParen,
    LBrace,
    RBrace,
    // Keywords, etc
    Function,
    Let,
    True,
    False,
    If,
    Else,
    Return,
}
