use crate::token::Token;
use std::iter::Peekable;
use std::str::Chars;

pub struct Lexer<'a> {
    file_name: &'a str,
    input: String,
    position: usize,
    read_position: usize,
    ch: char,
}

impl<'a> Lexer<'a> {
    pub fn new(file_name: &'a str, input: &str) -> Self {
        Lexer {
            file_name,
            input: input.to_string(),
            position: 0,
            read_position: 0,
            ch: '\0',
        }
    }
    pub fn next_token(&mut self) -> Token {
        todo!();
    }
    pub fn tokenize(&mut self) -> Vec<Token> {
        let mut peekable = self.input.chars().peekable();
        todo!();
    }
}
