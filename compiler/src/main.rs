#![warn(clippy::all, clippy::nursery, clippy::pedantic, clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
use std::env::Args;

use shared_lib::lexer::Lexer;
use shared_lib::token::Token;

// So here, all I would do is get the file names from the command line arguments, and then you can
// call the lexer on each file name. The lexer will return a vector of tokens, which you can then
// run over to get your AST. I would also recommend that you make a new struct for the command line arguments, so
// that you can iterate over them and call the lexer.tokenize() on each one.
// trying to do everything here would get super messy. I think you'll like the setup :)

fn main() {
    let args = std::env::args().skip(1).collect::<Vec<String>>();
    args.iter().for_each(|arg| {
        if arg.starts_with("-") {
            // Handle options here
        } else {
            let mut current_line: u32 = 1;

            let file_contents = match std::fs::read_to_string(&arg) {
                Ok(file_contents) => file_contents,
                Err(e) => panic!("Error compiling {arg}: {e}"),
            };

            for line in file_contents.lines() {
                let mut current_position: u32 = 0;

                for word in line.split(" ") {
                    tokens.push(token::Token {
                        line: line,
                        file_name: arg,
                        token: word,
                        line_number: current_line,
                        position_number: current_position,
                    });
                }

                current_line += 1;
            }
        }
    });

    println!("{tokens:?}");
}
